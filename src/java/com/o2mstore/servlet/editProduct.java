/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.o2mstore.servlet;

import com.o2mstore.beans.ToolServlets;
import com.o2mstore.ejb.ProductoFacade;
import com.o2mstore.jpa.Producto;
import com.o2mstore.jpa.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author masterinftel22
 */

@MultipartConfig
public class editProduct extends HttpServlet {
    @EJB
    private ProductoFacade productoFacade;
    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet editProduct</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet editProduct at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        String nameProd = request.getParameter("nameprod");
        
        request.setAttribute("nameprod", nameProd);
        RequestDispatcher rd = request.getRequestDispatcher("/content/html/editProduct.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        HttpSession miSesion= request.getSession(false);
        Usuario user = (Usuario) miSesion.getAttribute("user");
        
        String originalName = request.getParameter("originalname");
        String nombreP = request.getParameter("name");
        String subcategoria = request.getParameter("subcategoria");
        String descripcion = request.getParameter("descripcion");
        String datosImg = ToolServlets.saveImageOnDataBase(request,"imageProd");
        
        Producto prod = productoFacade.findByName(originalName);
        prod.setNombrep(nombreP);
        prod.setPropietario(user);
        prod.setDescripcion(descripcion);
        
        switch(subcategoria){
            case "android":
            case "blackbery":
            case "iphone":{
                prod.setCategoria("aplicaciones");
                prod.setSubcategoria(subcategoria);
                break;
            } 
            case "terror":
            case "comedia":
            case "accion":
            case "cienciaficcion":
            case "animacion":
            case "drama":
            case "thriller":{
                prod.setCategoria("peliculas");
                prod.setSubcategoria(subcategoria);
                break;
            }
            case "pop":
            case "rock":
            case "metal":
            case "clasica":
            case "hiphop":
            case "disco":{
                prod.setCategoria("musica");
                prod.setSubcategoria(subcategoria);
                break;
            }
            case "ciencias":
            case "cocina":
            case "historia":
            case "informatica":
            case "clasicos":
            case "infantil":{
                prod.setCategoria("libros");
                prod.setSubcategoria(subcategoria);
                break;
            }
            case "aplicaciones":{
                prod.setCategoria(subcategoria);
                prod.setSubcategoria("Sin Especificar");
                break;
            }
            case "pelicula":{
                prod.setCategoria(subcategoria);
                prod.setSubcategoria("Sin Especificar");
                break;
            }
            case "musica":{
                prod.setCategoria(subcategoria);
                prod.setSubcategoria("Sin Especificar");
                break;
            }
            case "libros":{
                prod.setCategoria(subcategoria);
                prod.setSubcategoria("Sin Especificar");
                break;
            }
        }
        
        prod.setImagen(datosImg);
        productoFacade.edit(prod);
        
        List <Producto> list = productoFacade.findByPropietary(user.getIdusuario());
        request.setAttribute("productos", list);
        RequestDispatcher rd = request.getRequestDispatcher("/content/html/profile.jsp");
        rd.forward(request, response);
        //response.sendRedirect("#");
                
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
