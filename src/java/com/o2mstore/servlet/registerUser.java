/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.o2mstore.servlet;

import com.o2mstore.beans.ToolServlets;
import com.o2mstore.ejb.UsuarioFacade;
import com.o2mstore.jpa.Usuario;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sun.misc.BASE64Encoder;

/**
 *
 * @author masterinftel21
 */

@MultipartConfig
public class registerUser extends HttpServlet {
    
    @EJB
    private UsuarioFacade usuarioFacade;
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String nombre = request.getParameter("name");
        String apellidos = request.getParameter("apellidos");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String email = request.getParameter("email");
        String date = request.getParameter("date");
        String terms = request.getParameter("terms");
        
        //Endode image from form
        String datosImg = ToolServlets.saveImageOnDataBase(request, "imageProf");
        
        
        Usuario user = usuarioFacade.findByEmail(email);
        
        if(user == null){
            if(password.equals(password2) && terms.equals("agree"))
            {
                user = new Usuario();
                
                Integer id = usuarioFacade.getMaxID();
                
                user.setIdusuario(id+1);
                user.setNombre(nombre);
                user.setApellidos(apellidos);
                user.setContrasena(password);
                user.setEmail(email);
                user.setFechanac(new Date());
                user.setRol("user");
                user.setImagen(datosImg);
                
                System.out.println(user);
                usuarioFacade.newUser(user);

                HttpSession miSesion= request.getSession(true);
                miSesion.setAttribute("user", user);
                //String emailSession = (String) miSesion.getAttribute("email");
                response.sendRedirect("indexStore.jsp");
            }
            else{
                //MOSTRAR QUE LAS CONTRASEÑAS NO COINCIDEN
                response.sendRedirect("indexStore.jsp");
            }
        }
        else{
            //MOSTRAR QUE YA HAY UN USUARIO CON ESE EMAIL
            response.sendRedirect("indexStore.jsp");
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
