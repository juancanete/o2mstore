/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**/package com.o2mstore.servlet;

import com.o2mstore.ejb.ProductoFacade;
import com.o2mstore.ejb.UsuarioFacade;
import com.o2mstore.jpa.Producto;
import com.o2mstore.jpa.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author VictorCG
 */
public class loginServlet extends HttpServlet {
    @EJB
    private ProductoFacade productoFacade;

    @EJB
    private UsuarioFacade usuarioFacade;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession miSesion= request.getSession(false);
        Usuario user = (Usuario) miSesion.getAttribute("user");
        
        request.setAttribute("user", user);
        List<Producto> list = productoFacade.findByPropietary(user.getIdusuario());
        request.setAttribute("productos", list);
        
        RequestDispatcher rd;
        
        rd = request.getRequestDispatcher("/content/html/profile.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession miSesion= request.getSession(true);
        
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        
        Usuario user = usuarioFacade.findByEmail(email);
        
        if(user != null){
            if(usuarioFacade.tryLogin(email, password)){

                miSesion.setAttribute("user", user);
                response.sendRedirect("indexStore.jsp");

            }
            else{
                
                request.setAttribute("login", "Contraseña erronea!!!");
                RequestDispatcher rd;

                rd = request.getRequestDispatcher("indexStore.jsp");
                rd.forward(request, response);
                
                
            }
        }
        else{
            
            request.setAttribute("login", "El e-mail no esta registrado!!!");
            RequestDispatcher rd;

            rd = request.getRequestDispatcher("indexStore.jsp");
            rd.forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
