/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.o2mstore.servlet;

import com.o2mstore.beans.ToolServlets;
import com.o2mstore.ejb.ProductoFacade;
import com.o2mstore.jpa.Producto;
import com.o2mstore.jpa.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author masterinftel21
 */
@MultipartConfig
public class productServlet extends HttpServlet {

    @EJB
    private ProductoFacade productoFacade;
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession miSesion= request.getSession(false);
        Usuario user = (Usuario) miSesion.getAttribute("user");
        
        String nombreP = request.getParameter("name");
        String subcategoria = request.getParameter("subcategoria");
        String descripcion = request.getParameter("descripcion");
        String  datosImg = ToolServlets.saveImageOnDataBase(request,"imageProd");
        Integer idProd = productoFacade.getMaxID();
        
        Producto prod = new Producto();
        prod.setIdproducto(idProd+1);
        prod.setNombrep(nombreP);
        
        
        prod.setPropietario(user);
        
        switch(subcategoria){
            case "appUnEspecified":
            case "android":
            case "blackberry":
            case "iphone":{
                prod.setCategoria("aplicaciones");
                prod.setSubcategoria(subcategoria);
                break;
            } 
            case "pelUnEspecified":
            case "terror":
            case "comedia":
            case "accion":
            case "cienciaficcion":
            case "animacion":
            case "drama":
            case "thriller":{
                prod.setCategoria("peliculas");
                prod.setSubcategoria(subcategoria);
                break;
            }
            case "musUnEspecified":
            case "pop":
            case "rock":
            case "metal":
            case "clasica":
            case "hiphop":
            case "disco":{
                prod.setCategoria("musica");
                prod.setSubcategoria(subcategoria);
                break;
            }
            case "libUnEspecified":
            case "ciencias":
            case "cocina":
            case "historia":
            case "informatica":
            case "clasicos":
            case "infantil":{
                prod.setCategoria("libros");
                prod.setSubcategoria(subcategoria);
                break;
            }
        }
        prod.setDescripcion(descripcion);
        prod.setFecha(new Date());
        prod.setPrecio(new BigDecimal(0));
        prod.setCalificacion(new Short("1"));
        prod.setDescargas(0);
        prod.setImagen(datosImg);
        productoFacade.create(prod);
        
        response.sendRedirect("indexStore.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
