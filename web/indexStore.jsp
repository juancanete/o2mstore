<%-- 
    Document   : indexStore
    Created on : Dec 12, 2013, 10:55:48 AM
    Author     : masterinftel22
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>O2M Store</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,700' rel='stylesheet' type='text/css'>

  <!-- Stylesheets -->
  <link href="content/style/bootstrap.css" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="content/style/font-awesome.css">
  <!-- Flex slider -->
  <link rel="stylesheet" href="content/style/flexslider.css">
  <!-- prettyPhoto -->
  <link rel="stylesheet" href="content/style/prettyPhoto.css">
  <!-- Main stylesheet -->
  <link href="content/style/style.css" rel="stylesheet">

  <!-- Bootstrap responsive -->
  <link href="content/style/bootstrap-responsive.css" rel="stylesheet">
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="content/img/favicon/favicon.png">
  
  <!-- JS-->
  <script src="content/js/jquery.js"></script>
  <script src="content/js/indexJS.js"></script>

</head>
<c:if test="${not empty login}">
    <body onload="cargarLogin('${login}');">
</c:if>
<body><c:if test="${empty login}">
    <body>
</c:if>

<!-- Navbar starts -->

  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container-fluid">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <div class="nav-collapse collapse">
          <ul class="nav pull-right">
            
              
            <c:if test="${empty user}">  
                <li><a href="#" onclick="cargarLogin();" class="br-purple"><i class="icon-user"></i> Log In </a></li>
                <li><a href="#" onclick="cargarReg();" class="br-purple"><i class="icon-user"></i> Registrar </a></li>
            </c:if>
                
            <c:if test="${not empty user}">  
                <li><a href="#" onclick="cargarProfile();" class="br-purple"><i class="icon-user"></i> Bienvenido ${user.nombre} ${user.apellidos}</a></li>
                <li><a href="#" onclick="cargarCarrito();" class="br-purple"><i class="icon-user"></i>Carrito de la compra</a></li>
                <li><a href="#" onclick="cargarProduct();" class="br-purple"><i class="icon-user"></i>Registrar un producto</a></li>
                <li><a href="/o2mstore/logout">Log Out</a></li>
            </c:if>
          </ul>
        </div>
      </div>
    </div>
  </div>

<!-- Navbar ends -->


<!-- Sliding box starts -->
   <div class="slide-box">
     
      <div class="padd">
        <div class="slide-box-button">
          <i class="icon-chevron-left"></i>
        </div>
       <h5>Redes Sociales</h5>
        Aquí puedes encontrar todas las redes sociales en las cuales estamos presentes, visitanos, siguenos. ;)
        <p>Información</p>
        <hr />

        <div class="social">
            <a href="https://www.facebook.com/pages/O2M-Store/620024354731403?fref=ts"><i class="icon-facebook facebook"></i></a>
            <a href="https://twitter.com/O2MStore1"><i class="icon-twitter twitter"></i></a>
            <a href="https://es.linkedin.com/"><i class="icon-linkedin linkedin"></i></a>
            <a href="https://accounts.google.com/"><i class="icon-google-plus google-plus"></i></a>
            <a href="https://www.pinterest.com/"><i class="icon-pinterest pinterest"></i></a>
        </div>
      
      </div>
    </div>

<!-- Sliding box ends -->    

<!-- Main content starts -->

<div class="content">

  <!-- Sidebar starts -->
  <div class="sidebar">

    <!-- Logo -->
    <div class="logo">
      <a href="#"><img src="content/img/logo4.png" alt="" /></a>
    </div>



        <div class="sidebar-dropdown"><a href="#">Navegacion</a></div>

        <!--- Sidebar navigation -->
        <!-- If the main navigation has sub navigation, then add the class "has_sub" to "li" of main navigation. -->

        <!-- Colors: Add the class "br-red" or "br-blue" or "br-green" or "br-orange" or "br-purple" or "br-yellow" to anchor tags to create colorful left border -->
        <div class="s-content">

          <ul id="nav">
            <!-- Main menu with font awesome icon -->
            <li><a href="indexStore.jsp" class="open br-red"><i class="icon-home"></i> Inicio</a>
              <!-- Sub menu markup 
              <ul>
                <li><a href="#">Submenu #1</a></li>
                <li><a href="#">Submenu #2</a></li>
                <li><a href="#">Submenu #3</a></li>
              </ul>-->
            </li>

            <li class="has_sub"><a href="#" class="br-blue"><i class="icon-list-alt"></i> Aplicaciones <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
              <ul>
                  <li><a onclick="servletData('android');" href="#">Android</a></li>
                <li><a onclick="servletData('blackberry');" href="#">Blackberry</a></li>
                <li><a onclick="servletData('iphone');" href="#">iPhone</a></li>
                <li><a onclick="servletData('appUnEspecified');" href="#">Sin Especificar</a></li>
               
              </ul>
            </li>  

            <li class="has_sub"><a href="#" class="br-green"><i class="icon-list-alt"></i>Películas<span class="pull-right"><i class="icon-chevron-right"></i></span></a>
              <ul>
                <li><a onclick="servletData('terror');" href="#">Terror</a></li>
                <li><a onclick="servletData('comedia');" href="#">Comedia</a></li>
                <li><a onclick="servletData('accion');" href="#">Accion</a></li>
                <li><a onclick="servletData('cienciaficcion');" href="#">Ciencia Ficción</a></li>
                <li><a onclick="servletData('animacion');" href="#">Animación</a></li>
                <li><a onclick="servletData('drama');" href="#">Drama</a></li>
                <li><a onclick="servletData('thriller');" href="#">Thriller</a></li>
                <li><a onclick="servletData('pelUnEspecified');" href="#">Sin Especificar</a></li>
              </ul>
            </li>              

            <li class="has_sub"><a href="#" class="br-orange"><i class="icon-comments"></i> Música <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
              <ul>
                <li><a onclick="servletData('pop');" href="#">Pop</a></li>
                <li><a onclick="servletData('rock');" href="#">Rock</a></li>
                <li><a onclick="servletData('metal');" href="#">Metal</a></li>
                <li><a onclick="servletData('clasica');" href="#">Clásica</a></li>
                <li><a onclick="servletData('hiphop');" href="#">Hip Hop</a></li>
                <li><a onclick="servletData('disco');" href="#">Disco</a></li>
                <li><a onclick="servletData('musUnEspecified');" href="#">Sin Especificar</a></li>
              </ul>
            </li> 
            
             <li class="has_sub"><a href="#" class="br-white"><i class="icon-comments"></i> Libros <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
              <ul>
                <li><a onclick="servletData('ciencias');" href="#">Ciencias</a></li>
                <li><a onclick="servletData('cocina');" href="#">Cocina</a></li>
                <li><a onclick="servletData('historia');" href="#">Historia</a></li>
                <li><a onclick="servletData('informatica');" href="#">Informática</a></li>
                <li><a onclick="servletData('clasicos');" href="#">Clásicos</a></li>
                <li><a onclick="servletData('infantil');" href="#">Infantil</a></li>
                <li><a onclick="servletData('libUnEspecified');" href="#">Sin Especificar</a></li>
              </ul>
            </li> 
            <li><a href="#" onclick="cargarAU();" class="br-purple"><i class="icon-user"></i> Quienes somos</a></li>
            <li><a href="#" onclick="cargarCU();" class="br-red"><i class="icon-envelope-alt"></i> Contactanos</a></li>
          </ul>

            <!-- Sidebar search -->
    

            <form action="javascript:buscarProd();" method="get" class="form-search s-widget">
              <div class="input-append">
                <input type="text" name="busqueda" id="busqueda" class="input-small search-query">
                <input type="submit" value="Buscar" name="btnBuscar" class="btn btn-danger">
              </div>
            </form>

        </div>



  </div>
  <!-- Sidebar ends -->

  <!-- Mainbar starts -->
  <div class="mainbar">

    <!-- Contact box starts -->

    <div class="cslider-box">
      <div class="cslider">
         <div class="container-fluid">
            <div class="row-fluid">
              <div class="span6">

                <div class="box-body">
                  <h6>Sobre nosotros</h6>
                  <hr />
                  <p><img src="content/img/logo4.png" alt="" width="150" class="alignleft"/> Somos <a href="#" onclick="cargarAU();" class="br-purple"> tres alumnos</a> del máster INFTEL, impartido en Málaga. <br>Esta página ha sido creada para completar el proyecto del módulo de JavaEE en el que se pedia crear una tienda virtual.</p>
                  <div class="clearfix"></div>
                 <hr />

                <div class="social">
                    <a href="https://www.facebook.com/pages/O2M-Store/620024354731403?fref=ts"><i class="icon-facebook facebook"></i></a>
                    <a href="https://twitter.com/O2MStore1"><i class="icon-twitter twitter"></i></a>
                    <a href="https://es.linkedin.com/"><i class="icon-linkedin linkedin"></i></a>
                    <a href="https://accounts.google.com/"><i class="icon-google-plus google-plus"></i></a>
                    <a href="https://www.pinterest.com/"><i class="icon-pinterest pinterest"></i></a>
                </div>

                 <hr />
                 <div class="address">
                    <div class="row-fluid">
                       <div class="span6">
                        <address>
                            <!-- Company name -->
                            <h6>ETSIT Málaga</h6>
                            <!-- Address -->
                            Bulevar Louis Pasteur, 35<br>
                            29071 Málaga<br>
                        </address>
                       </div>
                    </div>
                 </div>

                </div>  

              </div>
              <div class="span6">

                <div class="box-body">
                  
                  <h6>Contáctanos</h6>
                  <hr />
                  <div class="form">
                   <form class="form-horizontal">
                       <div class="control-group">
                         <label class="control-label" for="name">Nombre</label>
                         <div class="controls">
                           <input type="text" class="input-large" id="name">
                         </div>
                       </div>
                       <div class="control-group">
                         <label class="control-label" for="email">Email</label>
                         <div class="controls">
                           <input type="text" class="input-large" id="email">
                         </div>
                       </div>
                       <div class="control-group">
                         <label class="control-label" for="website">Página web</label>
                         <div class="controls">
                           <input type="text" class="input-large" id="website">
                         </div>
                       </div>
                       <div class="control-group">
                         <label class="control-label" for="comment">Comentario</label>
                         <div class="controls">
                           <textarea class="input-xlarge" id="comment" rows="4"></textarea>
                         </div>
                       </div>
                       <div class="form-actions">
                         <button type="submit" class="btn btn-danger">Enviar</button>
                         <button type="reset" class="btn">Reset</button>
                       </div>
                   </form>
                 </div>

                </div> 

              </div>
            </div>
         </div>
      </div>
      <div class="cslider-btn"><i class="icon-angle-down"></i></div>
    </div>

    <!-- Contact box ends -->
    <div class="errMsg">
        
    </div>

    <div class="matter">
        <div class="container-fluid">
            <div class="categories">
                <div class="row-fluid">

                    <div class="span12">            
                        <!-- Slider (Flex Slider) -->

                        <div class="box-body">
                            <div class="flexslider">
                                <ul class="slides">
                                    <!-- Each slide should be enclosed inside li tag. -->

                                    <!-- Slide #1 -->
                                    <li>
                                        <!-- Image -->
                                        <img src="content/img/photos/s2.png" alt=""/>
                                        <!-- Caption -->
                                        <div class="flex-caption">

                                            <!-- Title -->
                                            <h3>Tus aplicaciones</h3>
                                            <!-- Para -->
                                            <p>Una gran variedad de aplicaciones para tus dispositivos móviles de cualquier plataforma (Android, Iphone o BlackBerry)</p>
                                        </div>
                                    </li>

                                    <!-- Slide #2 -->
                                    <li>
                                        <img src="content/img/photos/portadaP.jpg" alt=""/>
                                        <div class="flex-caption">

                                            <h3>Películas</h3>
                                            <p>Todos los estrenos y clásicos del cine disponibles en un solo click.</p>
                                        </div>                  
                                    </li>

                                    <li>
                                        <img src="content/img/photos/portadaM.jpg" alt=""/>
                                        <div class="flex-caption">

                                            <h3>Música</h3>
                                            <p>Gran variedad de estilos y artistas donde encontrar la canción que te llegue al corazón.</p>
                                        </div>                  
                                    </li>
                                    
                                    <li>
                                        <img src="content/img/photos/portadaL.png" alt=""/>
                                        <div class="flex-caption">

                                            <h3>E-Books</h3>
                                            <p>Cansado de llenar estanterias con libros de papel?, en tu web o en tu móvil tendras la mayor de las bibliotecas.</p>
                                        </div>                  
                                    </li>



                                </ul>
                            </div>
                        </div>

                        <!-- Slider ends -->

                    </div>          
                </div>

            </div>
        

        <!-- Service ends -->



              <!-- Image blocks starts -->
              <div class="most-valued">
               
              </div>
              <!-- Image blocks ends -->

              <hr />


        <!-- Recent posts starts -->

        <div class="box-body recent-posts">
          

        </div>

        <!-- Recent posts ends -->

      </div>
    </div>

  </div>
  <!-- Mainbar ends -->
<div class="clearfix"></div>
  <!-- Foot starts -->             
    <div class="foot">
	  
      

    <div class="row-fluid">
      <div class="span12">
        <hr class="visible-desktop">
        <div class="copy">Copyright 2013 &copy; - <a href="#">O2MStore</a> - Designed by <a href="http://responsivewebinc.com/bootstrap-themes">Bootstrap Themes</a></div>
      </div>
    </div>
  <!-- Foot ends -->

</div>

<div class="clearfix"></div>

<!-- Main content ends -->



<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 
 
<!-- JS -->
    <script src="content/js/bootstrap.js"></script> <!-- Bootstrap -->
    <script src="content/js/filter.js"></script> <!-- Filter JS -->
    <script src="content/js/jquery.carouFredSel-6.1.0-packed.js"></script> <!-- CarouFredSel -->
    <script src="content/js/jquery.flexslider-min.js"></script> <!-- Flexslider -->
    <script src="content/js/jquery.isotope.js"></script> <!-- Isotope -->
    <script src="content/js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto -->
    <script src="content/js/jquery.tweet.js"></script> <!-- Tweet -->
    <script src="content/js/custom.js"></script> <!-- Main js file -->
    <script src="content/js/navigation.js"></script> <!-- Main js file -->
</body>
</html>