<%-- 
    Document   : editProfile
    Created on : 19-dic-2013, 2:27:35
    Author     : Juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <div class="span12">  

            <div class="well login-register">

                <h5>Editar ${nameprod}</h5>
                <hr />
                
                <iframe name="hiddenFrame" class="hide"></iframe>
                <div class="form">
                    <!-- Register product form -->
                    <form action="editProduct?originalname=${nameprod}" onsubmit="cargarProfile()" enctype='multipart/form-data' method="post" class="form-horizontal" target="hiddenFrame">
                        <!-- Name Producto-->
                        <div class="control-group">
                            <label class="control-label" for="name">Nombre Producto</label>
                            <div class="controls">
                                <input name="name" type="text" class="input-large" id="name" maxlength="30" required>
                            </div>
                        </div>
                        <!-- Categoria -->
                        <div class="control-group">
                            <label class="control-label" for="select">Categoria</label>
                            <div class="controls">                               
                                <select name="subcategoria" required>
                                    <option>&nbsp;</option>
                                    <option value="aplicaciones">Aplicaciones</option>
                                    <option value="android">--Android</option>
                                    <option value="blackberry">--Blackberry</option>
                                    <option value="iphone">--iPhone</option>
                                    <option value="pelicula">Película</option>
                                    <option value="terror">--Terror</option>
                                    <option value="comedia">--Comedia</option>
                                    <option value="accion">--Accion</option>
                                    <option value="cienciaficcion">--Ciencia Ficción</option>
                                    <option value="animacion">--Animación</option>
                                    <option value="drama">--Drama</option>
                                    <option value="thriller">--Thriller</option>
                                    <option value="musica">Música</option>
                                    <option value="pop">--Pop</option>
                                    <option value="rock">--Rock</option>
                                    <option value="metal">--Metal</option>
                                    <option value="clasica">--Clásica</option>
                                    <option value="hiphop">--Hip Hop</option>
                                    <option value="disco">--Disco</option>
                                    <option calue="libros">Libros</option>
                                    <option value="ciencias">--Ciencias</option>
                                    <option value="cocina">--Cocina</option>
                                    <option value="historia">--Historia</option>
                                    <option value="informatica">--Informática</option>
                                    <option value="clasicos">--Clásicos</option>
                                    <option value="infantil">--Infantil</option>
                                </select>  
                            </div>
                        </div>
                        <!-- Descripción -->
                        <div class="control-group">
                            <label class="control-label" for="descripcion">Breve descripción</label>
                            <div class="controls">
                                <input name="descripcion" type="descripcion" class="input-large" id="password" maxlength="100" required>
                            </div>
                        </div> 

                        <!--Select Image-->
                        <div class="control-group">
                            <div class="controls">
                                <input type="file" name="imageProd"/>
                            </div>
                        </div> 

                        <!-- Buttons -->
                        <div class="form-actions">
                            <!-- Buttons -->
                            <input type="submit" name="registrar" value="Registrar producto" class="btn btn-danger">
                            
                        </div>


                    </form>
                    <button onclick="closeProduct()" class="btn">Cerrar</button>
                </div> 


            </div>

        </div>
    </body>
</html>
