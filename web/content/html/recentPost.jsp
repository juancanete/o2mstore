<%-- 
    Document   : recentPost
    Created on : 16-dic-2013, 13:17:25
    Author     : David
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="rp" %>
<!DOCTYPE html>
<html>
    <body>
        
        <!-- Recent posts starts -->

        <div class="box-body recent-posts">
          <div class="mini-title">
            <!-- Title -->
            <h3>Últimos productos</h3>
          </div>

          <div class="row-fluid">
            <div class="span12">
              <div class="carousel_nav pull-left">
                <!-- Carousel navigation -->
                <a class="prev" id="car_prev" href="#"><i class="icon-chevron-left"></i></a>
                <a class="next" id="car_next" href="#"><i class="icon-chevron-right"></i></a>
              </div>
              <div class="clearfix"></div>
              <ul class="rps-carousel">
                  <!-- Recent posts #1 -->
                  <!-- Each carousel should be enclosed inside "li" tag -->
                  <rp:forEach var="prod" items="${product3}">

                  <li>
                      <div class="rp-item"> 
                        <!-- Image. -->
                        <div class="rp-image">        
                          <a href="#"><img src="data:image/gif;base64,${prod.imagen}"/></a>
                        </div>
                        <div class="rp-details">
                          <!-- Title and para -->
                          <h6><a href="#">${prod.nombrep}</a></h6>
                          <p>${prod.descripcion}</p>         
                        </div>                
                      </div>        
                  </li>
                </rp:forEach>                                                                                             
              </ul>

            </div>
          </div>

        </div>

        <!-- Recent posts ends -->
        
        
        
    </body>
</html>
