<%-- 
    Document   : register
    Created on : 16-dic-2013, 10:50:08
    Author     : masterinftel21
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <script src="content/js/indexJS.js"></script>
        <script src="content/js/jquery.js"></script>
        <script src="content/js/jquery.validate.js"></script>
        <script src="http://jquery.bassistance.de/validate/lib/jquery-1.9.0.js"></script>
    </head>    

    <body>

        <!-- Main content starts -->

        <div class="content">

            <div class="container-fluid">
                <div class="row-fluid">

                    <div class="span12">  

                        <div class="well login-register">

                            <h5>Registrarse como usuario</h5>
                            <hr />

                            <div class="form">
                                <!-- Register form (not working)-->
                                <form action="/o2mstore/registerUser" method="post" onsubmit="return comprobarContraseñas()" enctype='multipart/form-data' class="form-horizontal" name="f1" id="f1">
                                    <!-- Name -->
                                    <div class="control-group">
                                        <label class="control-label" for="name">Nombre</label>
                                        <div class="controls">
                                            <input type="text" name="name" class="input-large" id="name" maxlength="30" required><br>
                                        </div>
                                    </div>
                                    <!-- Apellidos -->
                                    <div class="control-group">
                                        <label  class="control-label" for="apellidos">Apellidos</label>
                                        <div class="controls">
                                            <input type="text" name="apellidos" class="input-large" id="apellidos" maxlength="50" required>
                                        </div>
                                    </div>
                                    <!-- Contraseña -->
                                    <div class="control-group">
                                        <label  class="control-label" for="password">Contraseña</label>
                                        <div class="controls">
                                            <input type="password" name="password" class="input-large" id="password" maxlength="20" required>
                                        </div>
                                    </div>
                                    <!-- Contraseña2 -->
                                    <div class="control-group">
                                        <label  class="control-label" for="password2">Repita la contraseña</label>
                                        <div class="controls">
                                            <input type="password" name="password2" class="input-large" id="password2" maxlength="20" required>
                                        </div>
                                    </div>
                                    <!-- Email -->
                                    <div class="control-group">
                                        <label  class="control-label" for="email">Email</label>
                                        <div class="controls">
                                            <input type="email" name="email" class="input-large" id="email" maxlength="40" required>
                                        </div>
                                    </div>                                   
                                    <!-- Fecha -->
                                    <div class="control-group">
                                        <label class="control-label" for="fechanac">Fecha de nacimiento</label>
                                        <div class="controls">
                                            <input type="date" name="date" class="input-large" id="fechanac">
                                        </div>
                                    </div>
                                    <!-- Checkbox -->
                                    <div class="control-group">
                                        <div class="controls">
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="terms" id="inlineCheckbox1" value="agree" required> Acepto los términos y condiciones
                                            </label>
                                        </div>
                                    </div> 

                                    <!--Select Image-->
                                    <div class="control-group">
                                        <div class="controls">
                                            <input type="file" name="imageProf"/>
                                        </div>
                                    </div> 

                                    <!-- Buttons -->
                                    <div class="form-actions">
                                        <!-- Buttons -->
                                        <input type="submit" name="registrar" value="Registrate" class="btn btn-danger">
                                        <button type="reset" class="btn">Reset</button>
                                    </div>
                                </form>
                                ¿Ya tienes cuenta? <a href="login.html">Login</a>
                            </div> 
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="clearfix"></div>

        <!-- Main content ends -->
        <script src="../js/custom.js"></script> <!-- Main js file -->
    </body>
</html>