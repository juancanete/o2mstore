<%-- 
    Document   : carrito
    Created on : 16-dic-2013, 10:50:46
    Author     : masterinftel21
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

    <body>

        <!-- Main content starts -->

        <div class="content">

            <div class="matter">
                <div class="container-fluid">

                    <!-- Title starts -->
                    <div class="page-title">
                        <h2>Carrito de la compra</h2>
                        <hr />
                    </div>
                    <!-- Title ends -->

                    <!-- Breadcrumb starts -->

                    <ul class="breadcrumb">
                        <li><a href="#">Home</a> <span class="divider">/</span></li>
                        <li><a href="#">Library</a> <span class="divider">/</span></li>
                        <li class="active">Data</li>
                    </ul>        

                    <!-- Breadcrumb ends -->

                    <hr />

                    <!-- Content starts -->

                    <div class="box-body">
                        <div class="row-fluid">

                            <div class="span4">

                                <div class="well">
                                    <!-- Your details -->
                                    <div class="address">
                                        <address>
                                            <h6>${user.nombre}</h6>
                                            ${user.apellidos}<br>
                                            ${user.rol}<br>
                                            <a href="mailto:#">${user.email}</a>
                                            <img src="data:image/gif;base64,${user.imagen}" width="120" height="120" alt="imagen"/>
                                        </address>
                                    </div>
                                </div>

                            </div>

                            
                             
                            <div class="mini-title">
                                <h3>PRODUCTOS EN EL CARRO</h3>
                            </div>
                            
                            <div class="row-fluid">
                                <table class="table">
                                    <tr class="tr">
                                        <th class="td">Nombre</th>
                                        <th class="td">Descripcion</th>
                                        <th class="td"></th>
                                    </tr>
                                <c:forEach var="prod" items="${product2}">
                                    
                                    
                                        <!-- Image block -->
                                        
                                            
                                                <!-- Link -->
                                                <a href="#">
                                                    <!-- Overlay content -->
                                                    
                                                        <tr class="tr">
                                                            <td class="td"><div class="image-big-text">${prod.nombrep}</div></td>
                                                            <td class="td"><div class="image-small-text">${prod.descripcion}</div></td>
                                                            <td class="td">
                                                                <button onclick="deleteProduct(${prod.idproducto})">Eliminar</button>
                                                            </td>
                                                        </tr>
                                                   
                                                            
                                                         
                                                </a>
                                            
                                        

                                    
                                </c:forEach>
                                </table>
                            </div>       
                                        
                                           <button onclick="finalizarPedido()">Finalizar</button>
                                        
                        </div>
                    </div>

                    <!-- Content ends -->

                </div>
            </div>


   
        <!-- Mainbar ends -->
        <div class="clearfix"></div>
   

    </div>

  

    <!-- Main content ends -->


    
</body>
</html>