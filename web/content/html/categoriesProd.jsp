<%-- 
    Document   : categoriesProd
    Created on : Dec 14, 2013, 6:03:00 PM
    Author     : masterinftel22
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <body>
        <div class="matter">
            <div class="container-fluid">

                <!-- Title starts -->
                <div class="page-title">
                    <h2>Catálogo</h2>
                    <p>Vea todos los productos disponibles</p>
                    <hr />
                </div>
                <!-- Title ends -->

                <!-- Breadcrumb starts -->

                <ul class="breadcrumb">
                    <li><a href="#">Inicio</a> <span class="divider">/</span></li>
                    <li><a href="#">Catalogo</a> <span class="divider">/</span></li>
                    <li class="active">${category}</li>
                </ul>        

                <!-- Breadcrumb ends -->

                <hr />

                <!-- Content starts -->
                <div class="span4">


                </div>
                <div class="box-body">
                    <div class="row-fluid">

                        <div class="span12">

                            <p>
                                <!-- Add filter names inside "data-filter". For example ".web-design", ".seo", etc., Filter names are used to filter the below images. -->
                            <div class="button">
                                <ul id="filters">
                                    <li><a href="#" data-filter="*" class="btn btn-danger">All</a></li>
                                    <li><a href="#" data-filter=".one" class="btn btn-danger">1 Star</a></li>
                                    <li><a href="#" data-filter=".two" class="btn btn-danger">2 Stars</a></li>
                                    <li><a href="#" data-filter=".three" class="btn btn-danger">3 Stars</a></li>
                                    <li><a href="#" data-filter=".four" class="btn btn-danger">4 Stars</a></li>
                                    <li><a href="#" data-filter=".five" class="btn btn-danger">5 Stars</a></li>
                                </ul>
                            </div>
                            </p>

                            <div id="portfolio" class="nocolor">

                                <!-- Add the above used filter names inside div tag. You can add more than one filter names. For image lightbox you need to include "a" tag pointing to image link, along with the class "prettyphoto".-->
                                <c:forEach var="prod" items="${product}">
                                    <c:choose>
                                        <c:when test="${prod.calificacion == 1}">
                                            <c:set var="numcateg" value="one"/>
                                        </c:when>
                                        <c:when test="${prod.calificacion == 2}">
                                            <c:set var="numcateg" value="two"/>
                                        </c:when>
                                        <c:when test="${prod.calificacion == 3}">
                                            <c:set var="numcateg" value="three"/>
                                        </c:when>
                                        <c:when test="${prod.calificacion == 4}">
                                            <c:set var="numcateg" value="four"/>
                                        </c:when>
                                        <c:when test="${prod.calificacion == 5}">
                                            <c:set var="numcateg" value="five"/>
                                        </c:when>
                                    </c:choose>

                                    <!-- Image block -->

                                    <div class="element ${numcateg}">
                                        <div class="box-body">
                                            <div class="block-image">
                                                <a>
                                                    <div class="image-overlay">
                                                        <div class="image-big-text">${prod.descripcion}</div>
                                                        <div class="image-small-text">${prod.nombrep}</div>
                                                        <button onclick="buyOneProduct('${prod.idproducto}')">Comprar</button>
                                                    </div>
                                                    <img src="data:image/gif;base64,${prod.imagen}">

                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </c:forEach>

                            </div>

                        </div>

                    </div>
                </div>

                <!-- Content ends -->

            </div>
        </div>



        <!-- Scroll to top -->
        <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 

        <!-- JS -->
        <script src="content/js/custom.js"></script> <!-- Main js file -->
    </body>
</html>
