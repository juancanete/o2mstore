<%-- 
    Document   : login
    Created on : 16-dic-2013, 10:49:03
    Author     : masterinftel21
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<!-- Navbar ends -->

<!-- Sliding box ends -->    

<!-- Main content starts -->
<body>
<div class="content">

  <div class="container-fluid">
    <div class="row-fluid">

      <div class="span12">  

                   <div class="well login-register">
                   
                       
                   <h5>Log In</h5>
                   <hr />
                   
                   <div class="errorMsg">
                   </div>

                    <form action="/o2mstore/loginServlet" method="post" class="form-horizontal">
                      <!-- Email -->
                      <div class="control-group">
                        <label class="control-label" for="inputEmail">Email</label>
                        <div class="controls">
                          <input type="email" name="email" id="email" placeholder="Email" maxlength="40" required>
                        </div>
                      </div>
                      <!-- Password -->
                      <div class="control-group">
                        <label class="control-label" for="inputPassword">Password</label>
                        <div class="controls">
                          <input type="password" name="password" id="password" placeholder="Password" required>
                        </div>
                      </div>
                      <!-- Remember me checkbox and sign in button -->
                      <div class="control-group">
                        <div class="controls">
                          <label class="checkbox">
                            <input type="checkbox"> Recuerdame
                          </label>
                          <br>
                          <input type="submit" name="login" value="Log In" class="btn btn-danger">
                          <button type="reset" class="btn">Reset</button>
                        </div>
                      </div>
                    </form>

                   </div>

      </div>

    </div>
  </div>

</div>


<script src="../js/custom.js"></script> <!-- Main js file -->
</body>
</html>